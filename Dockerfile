FROM            registry.gitlab.com/rustydb/docker/python
MAINTAINER      rusty@4lambda.io

ENV             PLANCK_RUNTIME test

# Setup the virtual env directory.
RUN             pip install virtualenv && virtualenv -p python36 /env
ENV             VIRTUAL_ENV=/env PATH=/env/bin:$PATH

# Install the application.
ADD             . /app
WORKDIR         /app
RUN             LC_CTYPE="en_US.UTF-8" python setup.py install && \
                pip install .[server]


# Launch gproxy!
EXPOSE          8080
ENTRYPOINT      ["uwsgi"]
CMD             ["app.ini"]
