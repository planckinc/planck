"""
Copyright:
2018 Planck Inc.; All Rights Reserved.

Description:
A Flask application for project management.
"""

from setuptools import find_packages
from setuptools import setup


def readme() -> str:
    """
    Returns the README file for a long description.
    :returns: Read file as a string.
    """
    with open('README.md') as readme_file:
        return str(readme_file)


setup(
    author='Russell Bunch',
    author_email='russell@4lambda.io',
    description='A Flask application for project management.',
    extras_require={
        'ci': [
            'tox',
        ],
        'lint': [
            'flake8',
        ],
        'unit': [
            'pytest',
            'pytest-cov',
        ],
        'server': [
            'uwsgi<=2.0.17',
        ],
    },
    include_package_data=True,
    install_requires=[
        'flask<1.1.0',
        'flask_bootstrap<3.4.0.0',
        'flask_login<0.5.0',
        'flask_nav==0.6',
        'flask_wtf<0.15.0',
        'pymodm<0.5.0',
    ],
    long_description=readme(),
    maintainer='Planck Inc.',
    maintainer_email='russell@4lambda.io',
    name='Planck',
    packages=find_packages(),
    url='https://gitlab.com/planckinc/planck',
    version='1.0.0',
    zip_safe=False,
)
