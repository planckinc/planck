from datetime import datetime

from planck.db import models
from tests.db.fixture import DBFixture


class ModelsTestCase(DBFixture):
    def setUp(self):
        super(ModelsTestCase, self).setUp()

    def test_user_modified(self):
        """Assert that the modified time auto-updates for a User."""
        for user in models.User.objects.all():
            self.assertEqual(user.created, user.modified)
            user.lname = user.lname
            user.save()
            self.assertNotEqual(user.created, user.modified)
            self.assertIsInstance(user.modified, datetime)
