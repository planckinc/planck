import json
import os
from unittest import TestCase

from pymongo import MongoClient

from planck.db import config
from planck.db import models


def purge() -> None:
    """
    Remove any and all databases created by this fixture. Will abort if database does not
    equal expected test database name.
    """
    if config.database != 'planck_test':
        raise AssertionError('Tests are not running against test database! Aborting.')
    client = MongoClient(config.mongodb)
    client.drop_database(config.database)
    client.close()


def plant_seeds() -> None:
    """
    Saves seed data into the database.
    """
    seeds_root = os.path.join(os.path.dirname(__file__), 'seeds')
    user_seeds = os.path.join(seeds_root, 'users.json')
    categories_seeds = os.path.join(seeds_root, 'categories.json')
    with open(user_seeds) as user_mocks:
        users = []
        for user in json.load(user_mocks):
            users.append(models.User(**user))
        models.User.objects.bulk_create(users)
    with open(categories_seeds) as categories_mocks:
        categories = []
        for category in json.load(categories_mocks):
            categories.append(models.Category(**category))
        models.Category.objects.bulk_create(categories)


class DBFixture(TestCase):
    def setUp(self):
        super(DBFixture, self).setUp()
        purge()
        config.connect_to()
        plant_seeds()
