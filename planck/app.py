"""
Copyright:
2018 Planck Inc.; All Rights Reserved.

Description:
The routes for the Planck app.
"""
from urllib.parse import urljoin
from urllib.parse import urlparse

from flask import Flask
from flask import Response
from flask import abort
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import url_for
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_login import current_user
from flask_login import login_required
from flask_login import login_user
from flask_login import logout_user
from flask_wtf.csrf import CSRFError
from flask_wtf.csrf import CSRFProtect
from pymodm.errors import ValidationError
from werkzeug.security import check_password_hash

from planck import forms
from planck.db import models
from planck.db.config import connect_to
from planck.logger import Logger

LOG = Logger(__name__)
connect_to(connect=False)
app = Flask(__name__)
app.secret_key = 'FAKE_SECRET_KEY'  # TODO: Make a real key.
app.static_folder = '../static'
app.template_folder = '../templates'
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.init_app(app)
login_manager.login_view = 'login'
Bootstrap(app)
CSRFProtect(app)


def is_safe_url(target: str) -> bool:
    """
    Determine if the target is safe.
    :param target: The target.
    :returns: Whether the target is safe.
    """
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


@login_manager.user_loader
def load_user(email: str) -> models.User:
    """
    Attempts to find the given email in the database.
    :param email: The email to search for.
    :returns: The User's account.
    """
    user = None
    try:
        user = models.User.objects.get({'_id': email})
    except models.User.DoesNotExist:
        LOG.debug(f'Could not find {email}.')
    return user


@app.route('/')
def index() -> Response:
    """
    The main page.
    :returns: A response.
    """
    return render_template('index.html')


@app.route('/dashboard', methods=['GET'])
@login_required
def dashboard() -> Response:
    """
    Show the overview of all categories and their tasks' statuses.
    :returns: A response showing categories and statuses.
    """
    categories = models.Category.objects.all()
    compiled = []
    for category in categories:
        if not category.tasks:
            continue
        sorted_tasks = {}
        for task in category.tasks:
            if task.status in sorted_tasks:
                sorted_tasks[task.status].append(task)
            else:
                sorted_tasks[task.status] = [task]
        compiled.append((category, sorted_tasks))
    return render_template('dashboard.html', categories=compiled, statuses=models.TASK_STATUSES)


@app.route('/login', methods=['GET', 'POST'])
def login() -> [Response, tuple]:
    """
    Provides a login page. The login page will attempt to login the user if found in the database.
    If the user does not exist or the data is bad then errors are returned to the login page.
    The user is finally logged into the session as current_user.
    :returns: A response with or without an error code.
    """
    form = forms.LoginForm()
    if request.method == 'POST' and form.validate():
        user = load_user(form.email.data)
        if not user:
            form.email.errors.append(f'No user "{form.email.data}" was found.')
            return render_template('forms/login.html', form=form), 401
        elif check_password_hash(user.hpass, form.password.data):
            login_user(user, remember=form.remember.data)
            target = request.args.get('next')  # Is this useful?
            if not is_safe_url(target):
                return abort(400)
            return redirect(target or url_for('dashboard'))
        else:
            form.password.errors.append('Incorrect')
            return render_template('forms/login.html', form=form), 401
    return render_template('forms/login.html', form=form)


@app.route('/logout')
def logout() -> Response:
    """
    Logs out the current_user.
    :returns: Initial index.
    """
    if current_user.is_authenticated:
        logout_user()
    return redirect(url_for('index'))


@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings() -> [Response, tuple]:
    """
    Provides a mechanism for resetting a password.
    :returns: The password verification page or an error code.
    """
    form = forms.VerifyPasswordForm()
    if request.method == 'POST':
        if not form.validate():
            return render_template('forms/settings.html', form=form)
        updated_user = forms.change_password(form, current_user)
        logout_user()
        login_user(updated_user)
        return redirect(url_for('dashboard'))
    return render_template('forms/settings.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup() -> [Response, tuple]:
    """
    Provides a signup page. The signup page will create a user in the database if the email is
    unique. If the user's data fails to validate or the email already exists then errors are
    returned. The user is logged into the session upon successful creation.
    :returns: A response with or without an error code.
    """
    form = forms.SignupForm()
    if request.method == 'POST' and form.validate():
        existing_user = load_user(form.email.data)
        if existing_user is None:
            try:
                user = forms.make_user(form)
            except ValidationError as error:
                for field, message in error.message.items():
                    getattr(form, field).errors.extend(message)
                return render_template('forms/signup.html', form=form), 400
            else:
                login_user(user)
                return redirect(url_for('dashboard'))
        else:
            form.email.errors.append(f'The email "{form.email.data}" already exists')
            return render_template('forms/signup.html', form=form), 409
    return render_template('forms/signup.html', form=form)


@app.route('/tasks', methods=['POST', 'GET'])
@login_required
def get_tasks() -> Response:
    """
    Returns the tasks page for the given logged in user.
    :returns: A response showing tasks.
    """
    form = forms.TasksForm()
    categories = models.Category.objects.all()
    if request.method == 'POST' and form.validate():
        if form.name.data:
            new_category = forms.make_category(form)
            if not new_category:
                # TODO: Return with errors.
                pass
            return jsonify(data=new_category.to_son())
    return render_template('tasks.html', form=form, categories=categories)


# TODO: Add 'GET' which returns tasks for just that category.
@app.route('/tasks/<string:category_id>', methods=['POST'])
@login_required
def new_task(category_id: str) -> Response:
    """
    Accept POST requests to make new Tasks for a given Category.
    :param category_id: The category to add the task to.
    :returns: A response containing the updated category.
    """
    form = forms.TasksForm()
    if form.validate():
        if form.what.data:
            updated_category = forms.make_task(form, category_id)
            if not updated_category:
                # TODO: Return with errors.
                pass
            return jsonify(data=updated_category.to_son())


@app.route('/favicon.ico')
def favicon() -> Response:
    """
    Sends the favicon securely under the "old de-facto" standard of serving the file.
    :returns: A response containing the favicon.
    """
    return send_from_directory(app.static_folder,
                               'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


@app.errorhandler(CSRFError)
def csrf_handler(error: CSRFError) -> tuple:
    """
    Handle CSRF problems by returning the csrf template.
    :param error: The CSRF error encountered.
    :returns: A response with the CSRF template.
    """
    return render_template('errors.html', csrf_error=error), 400
