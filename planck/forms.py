"""
Copyright:
2018 Planck Inc.; All Rights Reserved.

Description:
Forms using flask_wtf (wtforms) for templates.
"""
from flask_login import current_user
from flask_wtf import FlaskForm
from pymodm.errors import ValidationError
from pymongo.errors import DuplicateKeyError
from werkzeug.security import generate_password_hash
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import StringField

from planck.db import models


class VerifyPasswordForm(FlaskForm):
    """
    The Password Reset form.
    """
    password = PasswordField('Password')
    verify = PasswordField('Verify Password')

    def validate(self) -> bool:
        """
        Verify the password.
        :returns: Whether or not the two passwords match and are set.
        """
        super(VerifyPasswordForm, self).validate()
        if not self.verify.data:
            self.verify.errors.append('Missing')
        if not self.password.data:
            self.password.errors.append('Missing')
        if self.password.data and self.verify.data:
            if self.verify.data != self.password.data:
                self.password.errors.append('Mismatch')
            elif self.verify.data == self.password.data:
                return True
        return False


class LoginForm(FlaskForm):
    """
    The Login form.
    """
    email = StringField(models.User.email.verbose_name)
    password = PasswordField('Password')
    remember = BooleanField('Remember')


class SignupForm(VerifyPasswordForm):
    """
    The Signup form.
    """
    email = StringField(models.User.email.verbose_name)
    first_name = StringField(models.User.fname.verbose_name)
    last_name = StringField(models.User.lname.verbose_name)


class TasksForm(FlaskForm):
    """
    A form for adding a Task or Category.
    """
    name = StringField(models.Category.name.verbose_name)
    what = StringField(models.Task.what.verbose_name)


def change_password(form: SignupForm, user: models.User) -> models.User:
    """
    Changes the user's password based on the given form.
    :param form: A SignupForm.
    :param user: The user to have their password changed.
    :returns: The user's account after the password change.
    """
    new_password = generate_password_hash(form.verify.data)
    user.hpass = new_password
    return user.save()


def make_category(form: TasksForm) -> models.Category:
    """
    Creates a new Category in the database based on the TasksForm.
    :param form: a TasksForm.
    :returns: The created Category.
    """
    try:
        return models.Category(form.name.data).save(force_insert=True)
    except ValidationError as error:
        for field, message in error.message.items():
            getattr(form, field).errors.extend(message)
    except DuplicateKeyError:
        form.name.errors.extend(f'"{form.name.data}" already exists.')


def make_task(form: TasksForm, category_id: str) -> models.Category:
    """
    Updates a Category with a new task based on the TasksForm.
    :param form: a TasksForm.
    :param category_id: The ID of the Category to append the task to.
    :returns: The created task.
    """
    task = models.Task(author=current_user.email, what=form.what.data)
    category = models.Category.objects.get({'_id': category_id})
    category.tasks.append(task)
    try:
        category.save()
    except ValidationError as error:
        for field, message in error.message.items():
            getattr(form, field).errors.extend(message)
    else:
        return task


def make_user(form: SignupForm) -> models.User:
    """
    Creates a user in the database based on a given SignupForm.
    :param form: a SignupForm.
    :returns: The created User.
    """
    hashed_password = generate_password_hash(form.password.data)
    user_data = [form.email.data, form.first_name.data, form.last_name.data, hashed_password]
    return models.User(*user_data).save(force_insert=True)
