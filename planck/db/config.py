"""
Copyright:
2018 Planck Inc.; All Rights Reserved.

Description:
The database configuration for Planck.
"""
from os import environ

from pymodm import connect

from planck.logger import Logger

LOG = Logger(__name__)

runtime = environ.get('PLANCK_RUNTIME', 'test')
LOG.info(f'Running {runtime}')

if runtime == 'production':
    raise RuntimeError('Production environment has not been setup.')
elif runtime == 'development':
    username = None
    password = None
    database = 'planck_dev'
    location = 'db'
    porthole = 27017
else:
    username = None
    password = None
    database = 'planck_test'
    location = 'localhost'
    porthole = 27017

if username and password:
    mongodb = f'mongodb://{username}:{password}@{location}:{porthole}/{database}'
else:
    mongodb = f'mongodb://{location}:{porthole}/{database}'


def connect_to(uri: str = mongodb, **kwargs) -> None:
    """
    Connect to the given mongodb.
    :param uri: The URI to connect to if not the default.
    :param kwargs: Arguments to pass along to pymodm.connect.
    """
    connect(mongodb_uri=uri, **kwargs)
