"""
Copyright:
2018 Planck Inc.; All Rights Reserved.

Description:
The database models for Planck.
"""
import datetime

from flask_login import UserMixin
from pymodm import EmbeddedMongoModel
from pymodm import MongoModel
from pymodm import fields

TASK_STATUSES = [
    {
        'name': 'TODO',
        'css': 'progress-bar-danger',
    },
    {
        'name': 'In Progress',
        'css': 'progress-bar-warning',
    },
    {
        'name': 'Review',
        'css': 'progress-bar-info',
    },
    {
        'name': 'Done',
        'css': 'progress-bar-success',
    },
]


class User(MongoModel, UserMixin):
    """
    User document.
    """
    id = email = fields.EmailField(verbose_name='Email', primary_key=True)
    fname = fields.CharField(verbose_name='First Name', mongo_name='fname', required=True)
    lname = fields.CharField(verbose_name='Last Name', mongo_name='lname', required=True)
    hpass = fields.CharField(verbose_name='Hashed Password', mongo_name='hpass', required=True)
    notifications = fields.EmbeddedDocumentListField('Notification')
    created = fields.DateTimeField(default=datetime.datetime.utcnow())
    modified = fields.DateTimeField(default=created.default)

    def clean(self) -> None:
        """
        Ensures that modified is always updated.
        """
        self.modified = datetime.datetime.utcnow()

    def get_id(self) -> str:
        """
        Override for UserMixin.get_id() to return the email as the ID.
        :returns: The email.
        """
        return self.email


class Notification(MongoModel):
    """
    A Notification document.
    """
    user = fields.ReferenceField(User)
    data = fields.CharField()
    created = fields.DateTimeField(default=datetime.datetime.utcnow())
    modified = fields.DateTimeField(default=created.default)


class Category(MongoModel):
    """
    Category document.
    """
    name = fields.CharField(verbose_name='Name', primary_key=True)
    tasks = fields.EmbeddedDocumentListField('Task')
    created = fields.DateTimeField(default=datetime.datetime.utcnow())
    modified = fields.DateTimeField(default=created.default)

    def clean(self) -> None:
        """
        Ensures that modified is always updated.
        """
        self.modified = datetime.datetime.utcnow()


class Task(EmbeddedMongoModel):
    """
    Task document.
    """
    author = fields.ReferenceField(User, verbose_name='Author')
    category = fields.ReferenceField(Category)
    what = fields.CharField(verbose_name='What')
    who = fields.ReferenceField(User, verbose_name='Who')
    status = fields.CharField(default='TODO')
    created = fields.DateTimeField(default=datetime.datetime.utcnow())
    modified = fields.DateTimeField(default=created.default)

    def clean(self) -> None:
        """
        Ensures that modified is always updated.
        """
        # TODO: Validate status.
        # raise ValidationError(f'Invalid status {self.status}')
        self.modified = datetime.datetime.utcnow()
